import numpy as np
from scipy.optimize import linprog
import numpy.lib.recfunctions as recfunctions
from collections import defaultdict
import copy

class Solver(object):
    def __init__(self, n, m, C, edges):

        self.C = C

        self.n = n
        self.m = m

        self.ans = np.zeros(self.n)

        self.M = np.zeros(self.n * self.n, dtype=np.int32)
        self.M = self.M.reshape((self.n, self.n))

        self.A1 = np.zeros(self.n * self.m, dtype=np.int32)
        self.A1 = self.A1.reshape((self.m, self.n))

        self.A2 = np.zeros(self.n * self.n * self.m, dtype=np.int32)
        self.A2 = self.A2.reshape((self.m, self.n * self.n))

        self.B2 = np.zeros((self.n * self.n * self.n), dtype=np.int32)
        self.B2 = self.B2.reshape((self.n, self.n * self.n))

        self.b = np.ones(self.m, dtype=np.int32)

        self.deg_in = np.zeros(self.n, dtype=np.int32)
        self.deg_out = np.zeros(self.n, dtype=np.int32)

        self.v_in = defaultdict(list)
        self.v_out = defaultdict(list)

        self.cur_deg_in = np.zeros(self.n)

        self.graph = defaultdict(set)

        self.cur_order = np.zeros(self.n * self.n * (self.n - 1))
        self.cur_order = self.cur_order.reshape((self.n - 1, self.n * self.n))

        self.b_order = np.zeros(self.n - 1)

        for i in range(self.m):
            u = int(edges[i][0]) + 1
            v = int(edges[i][1]) + 1

            self.graph[u - 1].add(v - 1)

            self.cur_deg_in[v - 1] += 1

            self.M[u - 1][v - 1] = 1

            self.A1[i][u - 1] = 1
            self.A1[i][v - 1] = -1

            self.v_in[v - 1].append(u - 1)
            self.v_out[u - 1].append(v - 1)

            self.deg_in[v - 1] += 1
            self.deg_out[u - 1] += 1

            for k in range(self.n):
                self.A2[i][(u - 1) * self.n + k] = k
            for k in range(self.n):
                self.A2[i][(v - 1) * self.n + k] = -k

        self.c1 = self.deg_in - self.deg_out

        for i in range(self.n):
            for j in range(self.n):
                self.B2[i][i + j * self.n] = 1

        self.D2 = np.zeros((self.n * self.n * self.n), dtype=np.int32)
        self.D2 = self.D2.reshape((self.n, self.n * self.n))

        for i in range(self.n):
            for j in range(self.n):
                self.D2[i][i * self.n + j] = 1

        self.d = np.ones(self.n, dtype=np.int32)

        self.C2 = np.concatenate((self.A2, self.B2), axis=0)

        self.b1 = self.C * np.ones(self.n, dtype=np.int32)
        self.b2 = np.concatenate((self.b, (-1) * self.b1), axis=0)

        self.c2 = np.zeros(self.n * self.n, dtype=np.int32)
        for i in range(self.n):
            for j in range(self.n):
                self.c2[i * self.n + j] = (self.deg_in[i] - self.deg_out[i]) * (j + 1)

        self.min = self.m * self.n

    def get_layered_dict(self, cur_ans):

        cnt_l = dict()
        v_l = defaultdict(set)

        for i in range(self.n):

            if int(cur_ans[i]) in cnt_l:
                cnt_l[int(cur_ans[i])] += 1
                v_l[int(cur_ans[i])].add(i)

            else:

                cnt_l[int(cur_ans[i])] = 1
                v_l[int(cur_ans[i])] = set()
                v_l[int(cur_ans[i])].add(i)

        return cnt_l, v_l

    def linprog_in_greedy(self, A, b, c, index, cur_ans, cur_sets):

        for k in range(index):
            for v in cur_sets[k]:
                for i in range(self.m):
                    b[i] += A[i][v] * cur_ans[v]
                    A[i][v] = 0
                c[v] = 0

        cur = linprog(c, A, b_ub=(-1) * b, method="simplex").x
        cur -= np.min(cur)

        for v in range(index):
            for k in cur_sets[v]:
                cur[k] = cur_ans[k]

        return cur

    def linprog_in_search(self, C2, b2, c2, D2, d, cur_order, b_order, index, cur_sets):

        for v in range(index + 1):
            for k in cur_sets[v]:

                for i in range(self.m):
                    b2[i] += np.sign(C2[i][k * self.n + 1]) * v
                    C2[i][k * self.n:k * self.n + self.n] = np.zeros(self.n)

                c2[k * self.n:k * self.n + self.n] = np.zeros(self.n)

                l_num = v
                b2[self.m + l_num] += 1

                for i in range(self.n):
                    C2[self.m + i][k * self.n:k * self.n + self.n] = np.zeros(self.n)

                D2[k][k * self.n:k * self.n + self.n] = np.zeros(self.n)
                d[k] = 0

                for i in range(self.n - 1):
                    b_order[i] += np.sign(cur_order[i][k * self.n + 1]) * v
                    cur_order[i][k * self.n:k * self.n + self.n] = np.zeros(self.n)

        for v in range(index + 1, len(cur_sets)):
            for k in cur_sets[v]:
                cur_rct = np.zeros(self.n * self.n)
                cur_rct[k * self.n: k * self.n + self.n] = (-1) * np.arange(self.n)

                C2 = np.concatenate((C2, [cur_rct]), axis=0)
                b2 = np.append(b2, [index])
        C2 = np.concatenate((C2, cur_order))
        b2 = np.concatenate((b2, b_order))
        res = linprog(c2, A_ub=C2, b_ub=(-1) * b2, A_eq=D2, b_eq=d, bounds=[0, 1], method='simplex').x

        for v in range(index + 1):
            for k in cur_sets[v]:
                res[k * self.n:k * self.n + self.n] = np.zeros(self.n)
                res[k * self.n + v] = 1
        return res

    def greedy_separate(self, index, cur_ans, cur_sets):

        cur = self.linprog_in_greedy(self.A1.copy(), self.b.copy(), self.c1.copy(), index, cur_ans, cur_sets)

        cnt_l, v_l = self.get_layered_dict(cur)

        shift = 0
        for i in sorted(cnt_l.keys()):
            if cnt_l[i] > self.C:

                temp_v = np.array([])
                temp_deg = np.array([])
                for j in v_l[i]:
                    temp_v = np.append(temp_v, j)
                    temp_deg = np.append(temp_deg, self.deg_out[j] - self.deg_in[j])

                items = recfunctions.merge_arrays([temp_deg, temp_v])
                items.dtype.names = ['deg', 'v']
                items.sort(order=['deg'])

                l_i = cur[int(temp_v[0])]
                k = 0

                for j in range(np.size(temp_v)):
                    cur[int(items[j][1])] = l_i + (k // self.C) + shift
                    k += 1

                shift += (cnt_l[i] - 1) // self.C
            else:
                for j in v_l[i]:
                    cur[j] += shift

        temp_min = cur @ self.c1
        if self.min > temp_min:
            self.min = temp_min
            self.ans = cur

    def greedy_push(self, index, cur_ans, cur_sets):

        cur = self.linprog_in_greedy(self.A1.copy(), self.b.copy(), self.c1.copy(), index, cur_ans, cur_sets)

        cnt_l, v_l = self.get_layered_dict(cur)

        for i in sorted(cnt_l.keys()):
            if cnt_l[i] > self.C:
                temp_v = np.array([])
                temp_deg = np.array([])
                for j in v_l[i]:
                    temp_v = np.append(temp_v, j)
                    temp_deg = np.append(temp_deg, self.deg_out[j] - self.deg_in[j])
                items = recfunctions.merge_arrays([temp_deg, temp_v])
                items.dtype.names = ['deg', 'v']
                items.sort(order=['deg'])
                for j in reversed(range(np.size(temp_v))):
                    if cnt_l[i] > self.C:
                        self.push(cur, int(items[j][1]), cnt_l, v_l)
                    else:
                        break
        temp_min = cur @ self.c1
        if self.min > temp_min:
            self.min = temp_min
            self.ans = cur

    def push(self, cur, v, cnt_l, v_l):

        temp_v_l = copy.deepcopy(v_l[cur[v] + 1])

        for u in temp_v_l:
            if self.M[v][u] == 1:
                self.push(cur, u, cnt_l, v_l)

        v_l[int(cur[v])].remove(v)
        cnt_l[int(cur[v])] -= 1

        cur[v] = cur[v] + 1

        if int(cur[v]) in cnt_l:
            cnt_l[int(cur[v])] += 1
            v_l[int(cur[v])].add(v)

        else:
            cnt_l[int(cur[v])] = 1
            v_l[int(cur[v])] = set()
            v_l[int(cur[v])].add(v)

    def solve(self):
        self.make_search()
        return self.ans, self.min

    def search(self, cur_sets, index, cur_ans):

        if index == len(cur_sets) - 1:
            cur = cur_ans @ self.c2
            if self.min > cur:
                self.min = cur
                self.ans = cur_ans.reshape((self.n, self.n)) @ np.arange(self.n)
            return

        res = self.linprog_in_search(self.C2.copy(), self.b2.copy(), self.c2.copy(), self.D2.copy(), self.d.copy(),
                                     self.cur_order.copy(), self.b_order.copy(), index, cur_sets)

        for v in range(index + 1):
            for k in cur_sets[v]:
                res[k * self.n:k * self.n + self.n] = np.zeros(self.n)
                res[k * self.n + v] = 1

        temp_min = res @ self.c2
        if temp_min >= self.min:
            return

        self.greedy_push(index + 1, np.copy(cur_ans).reshape((self.n, self.n)) @ np.arange(self.n), cur_sets)
        f = True
        for v in cur_sets[index + 1]:
            for u in self.v_in[v]:
                if u in cur_sets[index]:
                    f = False
                    break
            if not f:
                break

        if f and len(cur_sets[index + 1]) + len(cur_sets[index]) <= self.C:
            temp_sets = copy.deepcopy(cur_sets)
            temp_ans = np.copy(cur_ans)

            for j in range(index + 1, len(cur_sets)):
                for v in cur_sets[j]:
                    temp_ans[v * self.n + j] = 0
                    temp_ans[v * self.n + j - 1] = 1

            temp_sets[index] |= cur_sets[index + 1]
            temp_sets.pop(index + 1)

            self.search(temp_sets, index, temp_ans)

        self.search(copy.deepcopy(cur_sets), index + 1, np.copy(cur_ans))

    def topsorts(self, visited, sorts):

        flag = False

        for i in range(self.n):
            if self.cur_deg_in[i] == 0 and not visited[i]:

                for k in self.graph[i]:
                    self.cur_deg_in[k] -= 1

                sorts.append(i)
                visited[i] = True
                self.topsorts(visited, sorts)

                visited[i] = False
                sorts.pop()
                for k in self.graph[i]:
                    self.cur_deg_in[k] += 1

                flag = True

        if not flag:
            self.cur_cnt += 1
            print(self.cur_cnt)
            cur_sets = [{sorts[i]} for i in range(self.n)]
            cur_ans = np.zeros(self.n * self.n)

            for i in range(self.n):
                cur_ans[sorts[i] * self.n + i] = 1

            self.cur_order = np.zeros(self.n * self.n * (self.n - 1))
            self.cur_order = self.cur_order.reshape((self.n - 1, self.n * self.n))
            self.b_order = np.zeros(self.n - 1)

            for i in range(self.n - 1):
                self.cur_order[i][self.n * sorts[i]:self.n * sorts[i] + self.n] = np.arange(self.n)
                self.cur_order[i][self.n * sorts[i + 1]:self.n * sorts[i + 1] + self.n] = (-1) * np.arange(self.n)

            self.search(copy.deepcopy(cur_sets), 0, np.copy(cur_ans))

    def make_search(self):
        sorts = list()
        visited = [False] * self.n
        self.cur_cnt = 0
        self.topsorts(visited, sorts)
        return self.min, self.ans
