from random import sample
from collections import defaultdict

class GraphGenerator(object):
    def sample_graph(self):
        points = list()
        for i in range(self.n):
            for j in range(i + 1, self.n):
                points.append((i, j))
        edges = sample(points, k=self.m)
        graph = defaultdict(set)
        for e in edges:
            graph[e[0]].add(e[1])
            graph[e[1]].add(e[0])
        return graph, edges
    def dfs(self, graph, start, visited):
        visited.add(start)
        for next in graph[start] - visited:
            self.dfs(graph, next, visited)
    def is_connected(self, graph):
        visited = set()
        self.dfs(graph, 0, visited)
        for i in range(self.n):
            if i not in visited:
                return False
        return True
    def generate(self, n, m, connected=True):
        self.n = n
        self.m = m
        graph, edges = self.sample_graph()
        if not connected:
            return edges
        while not self.is_connected(graph):
            graph, edges = self.sample_graph()
        return edges
